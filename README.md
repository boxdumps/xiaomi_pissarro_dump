## pissarro-user 11 RP1A.200720.011 V12.5.6.0.RKTINFK release-keys
- Manufacturer: xiaomi
- Platform: mt6877
- Codename: pissarro
- Brand: Xiaomi
- Flavor: pissarro-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: V12.5.6.0.RKTINFK
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Xiaomi/pissarro/pissarro:11/RP1A.200720.011/V12.5.6.0.RKTINFK:user/release-keys
- OTA version: 
- Branch: pissarro-user-11-RP1A.200720.011-V12.5.6.0.RKTINFK-release-keys-random-text-14675190510243
- Repo: xiaomi_pissarro_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
